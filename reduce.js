let myReduce = (arr, cb , startPoint) => {
  if (!arr || arr.length == 0) return [];
  let accumulator; 
  let startIdx = 0;
   if(startPoint !== undefined){
     accumulator = startPoint;
     startIdx = 0;
   }else{
     accumulator = arr[0];
     startIdx = 1;
   }
  if (cb instanceof Function) {
    for (let i = startIdx; i < arr.length; i++) {
      accumulator = cb(accumulator, arr[i] , i , arr);
    }
    return accumulator;
  }
  return [];
};

module.exports = myReduce;
