let myforEach = (arr, cb) => {
  if (!arr || arr.length === 0) return [];

  if (cb instanceof Function) {
    for (let i = 0; i < arr.length; i++) {
      cb(arr[i], i);
    }
  } else {
    return [];
  }
};

module.exports = myforEach;
