const myFind = (arr, cb) => {
  if (!arr || arr.length == 0) return [];

  if (cb instanceof Function) {
    for (let i = 0; i < arr.length; i++) {
      if (cb(arr[i])) return arr[i];
    }
  }
  return [];
};

module.exports = myFind;
