const myFilter = require("../filter");

const items = [1, 2, 3, 4, 5, 5];

let ans = myFilter(items, (item) => {
  return item > 6;
});

console.log(ans);
