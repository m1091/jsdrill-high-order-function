const myFind = require("../find");

const items = [1, 2, 3, 4, 5, 5];

let ans = myFind(items, (item) => {
  return item === 8;
});

console.log(ans);
