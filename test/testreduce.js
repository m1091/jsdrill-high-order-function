const myReduce = require("../reduce");
const items = [1, 2, 3, 4, 5, 5];

let ans = myReduce(items, (total, currentvalue) => {
  return total + currentvalue;
});

console.log(ans);
