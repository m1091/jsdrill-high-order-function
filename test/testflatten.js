const myFlatten = require("../flatten");
const nestedArray = [1, [2], [[3]], [[[4]]]];

let ans = myFlatten(nestedArray);

console.log(ans);
