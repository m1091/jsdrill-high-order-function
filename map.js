let myMap = (arr, logic) => {
  let newArr = [...arr]
  let output = [];
  if (!arr || arr.length == 0) return [];
  if (logic instanceof Function) {

    for(let i =0 ; i<arr.length ; i++){
      if(arr[i] )
      newArr[i] = parseInt(arr[i]);
    } 

    for (let i = 0; i < arr.length; i++) {
      let returnedAns = logic(arr[i] , i , arr);
            output.push(returnedAns);
    }
    return output;
  }
  return [];
};

module.exports = myMap;
