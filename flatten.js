const myFlatten = (arr) => {
  let output = [];

  if (!arr || arr.length == 0) return [];

  function helper(array) {
    for (let i = 0; i < array.length; i++) {
      if (Array.isArray(array[i])) {
        helper(array[i]);
      } else {
        output.push(array[i]);
      }
    }
  }
  helper(arr);
  return output;
};

module.exports = myFlatten;
