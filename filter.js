const myFilter = (arr, cb) => {
  if (!arr || arr.length == 0) return [];
  let output = [];
  if (cb instanceof Function) {
    for (let i = 0; i < arr.length; i++) {
      if (cb(arr[i])) output.push(arr[i]);
    }
    return output;
  }
  return [];
};

module.exports = myFilter;
